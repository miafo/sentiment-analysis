import pandas as pd
import numpy as np
from keras.models import Sequential
from keras.layers.core import Flatten, Dense, Dropout
from keras.layers.convolutional import Convolution2D, MaxPooling2D, ZeroPadding2D
from keras.optimizers import SGD
import cv2
from tensorflow.keras.applications import resnet50
from keras.applications.resnet50 import preprocess_input, decode_predictions
from keras.preprocessing import image
import torch
import torch.utils.data
import matplotlib.pyplot as plt
import os
from data.export_train_label import creat_label
from data.image_downloader import download_img

filename = 'fer2013.csv'

#Read csv
emotion_data = pd.read_csv(filename)
print(emotion_data)

##Convert csv to images
def save_fer_img():
    for index,row in emotion_data.iterrows():
        pixels=np.asarray(list(row['pixels'].split(' ')),dtype=np.uint8)
        img=pixels.reshape((48,48))
        pathname=os.path.join('fer_images',str(index)+'.jpg')
        cv2.imwrite(pathname,img)
        print('image saved as {}'.format(pathname))


#save_fer_img()

face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_alt.xml')
eyePair_cascade = cv2.CascadeClassifier('haarcascade_mcs_eyepair_big.xml')

def return_eye_pair(infile, outfile):
    #print(infile, outfile)
    img = cv2.imread(infile)
    #img = resizeimage.resize_cover(img, [200, 100],  validate=False)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)


    faces = face_cascade.detectMultiScale(gray, 1.1, 3)
    if len(faces) == 0: return
    for x,y,w,h in faces:
    	roi_gray = gray[y:y+h, x:x+w]
    	roi_color = img[y:y+h, x:x+w]
    	eyes = eyePair_cascade.detectMultiScale(roi_gray)	
    	if len(eyes) == 0: return	
    	for (ex,ey,ew,eh) in eyes:
    		eyes_roi = roi_color[ey: ey+eh, ex:ex + ew]
			#cv2.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),(0,255,0),2)

    cv2.imwrite(outfile, eyes_roi)
	
	


def load_images_from_folder(folder):
    outFolder = 'output'
    for filename in os.listdir(folder):
        img = cv2.imread(os.path.join(folder,filename))
        if img is not None:
            #print("hei")
            return_eye_pair(os.path.join(folder,filename), os.path.join('output',filename))
            


#load_images_from_folder('fer_images')

emotions = ['Angry', 'Disgust', 'Fear', 'Happy', 'Sad', 'Surprise', 'Neutral']


# split to 3 parts
groups = [g for _, g in emotion_data.groupby('Usage')]
training_data = groups[1]
validation_data = groups[2]
testing_data = groups[0]
device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

def make_dataloader(data, batch_size, shuffle):
    images, labels = data['pixels'], data['emotion']
    images = np.array([np.fromstring(image, np.uint8, sep=' ') for image in images]) / 255.0 # normalizing data to be between 0 and 1
    images = torch.FloatTensor(images.reshape(images.shape[0], 1, 48, 48)).to(device) # 1 color channel, 48x48 images
    dataset = torch.utils.data.TensorDataset(images, torch.LongTensor(np.array(labels)).to(device))
    return torch.utils.data.DataLoader(dataset=dataset, batch_size=batch_size, shuffle=shuffle)


train_loader = make_dataloader(training_data, 100, True)
valid_loader = make_dataloader(validation_data, 100, False)

dataiter = iter(train_loader)
images, labels = dataiter.next()
print(emotions[labels[2]])
plt.imshow(images[2].view(48, 48).cpu())




def main():

    # Using google facial expression dataset
    if not os.path.exists('data/train'):
        os.makedirs('data/train', exist_ok=True)
        creat_label()
        download_img()

    folder = 'data/train'
    load_images_from_folder(folder)

 
if __name__ == '__main__':
    main()